require 'sinatra'
require 'rdiscount'

set :markdown, :layout_engine => :erb, :layout => :title

get '/' do
	
	# Load This many Posts per page
	@posts_per_page = 10

	# get the current date
	@time = Time.now
	@year = @time.year
	@month = @time.month
	@day = @time.day

	

	# now count backwards and load 10 posts
	@my_articles = Array.new
	@post_count = 0
	until @post_count > @posts_per_page  do
		@y = "%04d" % @year
		@m = "%02d" % @month
		@d = "%02d" % @day
		if File.directory?("views/posts/" + @y + "/" + @m + "/" + @d)
			Dir.glob("views/posts/" + @y + "/" + @m + "/" + @d + "/*.md") do |my_text_file|
  				@full_path = my_text_file
  				@article = RDiscount.new(File.open(@full_path).read).to_html
  				
  				@title = @full_path.split('/')
			  	@title = @title[@title.length-1]
			  	@title = @title.split('.')
			  	@title = @title[0]

  				@article_title_start = @article.index('<h1>') + 4
				@article_title_end = @article.index('</h1>') - 1
				@article_title = @article[@article_title_start..@article_title_end]
				@article[@article_title_start..@article_title_end] = 
					'<a href=posts/' + @y + '/' + @m + '/' + @d + '/' + @title + '>' + @article_title + '</a>'


  				@my_articles.push @article
  				@post_count +=1
  				@day = @day - 1
  			end
		else
			if @day > 1
				@day = @day - 1
			else
				@day = 31
				if @month > 1
					@month = @month - 1
				else
					@month = 12
					if @year > 2000
						@year = @year - 1
					else
						@post_count = @post_count + @posts_per_page + 1
					end
				end
			end
		end
	end	

	erb :blog
end

get '/posts/:year/:month/:day/:title' do 
	@year = params[:year]
	@month = params[:month]
	@day = params[:day]
	@title = params[:title]

	@article = RDiscount.new( File.open("views/posts/" + 
		@year + "/" + @month + "/" + @day + "/" + @title + ".md").read ).to_html

	@article_title_start = @article.index('<h1>') + 4
	@article_title_end = @article.index('</h1>') - 1
	@article_title = @article[@article_title_start..@article_title_end]
	@article[@article_title_start..@article_title_end]= '<a href=' + @title + '>' + @article_title + '</a>'

	erb :post
end

get '/pages/:page' do
	@page = params[:page]
	@page = RDiscount.new( File.open("views/pages/" + @page + ".md").read ).to_html
	erb :page
end

# get '/pages/projects' do
# 	erb 'pages/projects'.to_sym
# end

# get '/pages/about' do
# 	erb 'pages/projects'.to_sym
# end

# get '/pages/archive' do
# 	erb 'pages/projects'.to_sym
# end

# get '/pages/:page' do
# 	erb 'pages/archive'.to_sym
# end